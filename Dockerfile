FROM eclipse-temurin:17-jre-jammy
ENV TZ="Asia/Almaty" JAVA_FLAGS="-Xmx500m"
WORKDIR /app
EXPOSE 8080
COPY target/*.jar example-service.jar
ENTRYPOINT ["sh", "-c", "java -jar example-service.jar $JAVA_FLAGS"]
