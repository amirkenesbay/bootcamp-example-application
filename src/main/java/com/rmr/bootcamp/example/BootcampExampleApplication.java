package com.rmr.bootcamp.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootcampExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootcampExampleApplication.class, args);
    }

}
